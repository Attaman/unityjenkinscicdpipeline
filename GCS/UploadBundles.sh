echo "Uploading bundles to Google cloud storage..."
BUNDLE_PATH=$(head -n 1 $OUTPUT_PATH/BundleResult.txt)
echo $BUNDLE_PATH
echo $GCS_BUNDLES_ENDPOINT/$BUILD_TARGET
gsutil cp -r $BUNDLE_PATH $GCS_BUNDLES_ENDPOINT/$BUILD_TARGET
