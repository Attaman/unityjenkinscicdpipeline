echo ''
echo 'Build Unity...' 
echo ''

$UNITY_PATH -batchmode -nographics -quit -projectPath $PROJECT_PATH -executeMethod $BUILD_SCRIPT -buildTarget $BUILD_TARGET -buildMode $BUILD_MODE -buildOutputPath $BUILD_RESULT_PATH -buildNumber $BUILD_NUMBER
if [ $? -ne 0 ]; then
echo ''
echo 'Build failed!'
echo '' 
exit 1
fi
echo ''
echo 'Unity Build Succeed!'
echo '' 
