echo ''
echo 'Archive Xcode...' 
echo ''
xcodebuild -workspace "$BUILD_RESULT_PATH/Unity-iPhone.xcworkspace" -scheme "Unity-iPhone" archive -archivePath "$ARCHIVE_PATH/Unity-iPhone.xcarchive"
if [ $? -ne 0 ]; then
echo ''
echo 'Xcode archive failed!'
echo '' 
exit 1
fi
echo ''
echo 'Archive Xcode completed' 
echo ''