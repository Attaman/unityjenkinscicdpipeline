echo ''
echo 'Export ipa...' 
echo '' 
xcodebuild -exportArchive -archivePath "$ARCHIVE_PATH/Unity-iPhone.xcarchive" -exportOptionsPlist "BuildTool/IOS/AppStoreOptions.plist" -exportPath $ARCHIVE_PATH -allowProvisioningUpdates
if [ $? -ne 0 ]; then
echo ''
echo 'Xcode export failed!'
echo '' 
exit 1
fi
echo ''
echo 'Export Xcode completed' 
echo ''
