echo ''
echo 'Build Xcode...' 
echo ''
xcodebuild -workspace "$BUILD_RESULT_PATH/Unity-iPhone.xcworkspace" -scheme "Unity-iPhone"
if [ $? -ne 0 ]; then
echo ''
echo 'Xcode build failed!'
echo '' 
exit 1
fi
echo ''
echo 'Build Xcode completed' 
echo ''
