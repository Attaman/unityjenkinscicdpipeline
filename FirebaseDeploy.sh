echo ''
echo 'Firebase distribution...' 
echo ''

firebase appdistribution:distribute $ARTIFACT_PATH \
  --app $APP_ID \
  --groups "Internal"
if [ $? -ne 0 ]; then

echo ''
echo 'Firebase distribution failed!'
echo '' 
exit 1
fi
