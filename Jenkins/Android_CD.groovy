pipeline{
    agent any
    environment {
        PROJECT_PATH="${env.WORKSPACE}/Zombie-hero"
        BUILD_SCRIPT="BuildTool.BuildFullPlayer"
        BASE_UNITY_PATH="/Applications/Unity/Hub/Editor"
        UNITY_VERSION="2021.3.8f1"
        UNITY_PATH="${BASE_UNITY_PATH}/${UNITY_VERSION}/Unity.app/Contents/MacOS/Unity"

        BUILD_TARGET="Android"
	    BUILD_NUMBER="${env.BUILD_NUMBER}"

        OUTPUT_PATH="${PROJECT_PATH}/Builds/${BUILD_TARGET}/${BUILD_MODE}"
        LOG_PATH="${OUTPUT_PATH}/Logs"
        BUILD_RESULT_PATH="${OUTPUT_PATH}/UnityBuildOutput"

        GCS_BUNDLES_ENDPOINT="gs://zombiescape/Bundles"
        
        ARTIFACT_PATH="${BUILD_RESULT_PATH}/Zombiescape.apk"
        APP_ID="1:1074248512513:android:85bdce789d5a1f53193c00"
    }

    stages{
        stage('Prepare'){
            steps {
               sh '''
                   mkdir -p $OUTPUT_PATH
               '''
            }
        }
        stage('Build Unity'){
            steps {
               sh '''
                   sh BuildUnityPlayer.sh
               '''
            }
        }
        stage('Upload bundles'){
            steps {
               sh '''
                   sh GCS/UploadBundles.sh
               '''
            }
        }
        stage('Build deploy'){
            steps {
               sh '''
                   sh FirebaseDeploy.sh
               '''
            }
        }
    }
}
