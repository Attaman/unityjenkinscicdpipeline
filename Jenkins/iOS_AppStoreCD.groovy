pipeline{
    agent any
    environment {
        PROJECT_PATH="${env.WORKSPACE}/Zombie-hero"
        BUILD_SCRIPT="BuildTool.BuildFullPlayer"
        BASE_UNITY_PATH="/Applications/Unity/Hub/Editor"
        UNITY_VERSION="2021.3.8f1"
        UNITY_PATH="${BASE_UNITY_PATH}/${UNITY_VERSION}/Unity.app/Contents/MacOS/Unity"

        BUILD_TARGET="iOS"
	    BUILD_NUMBER="${env.BUILD_NUMBER}"

        OUTPUT_PATH="${PROJECT_PATH}/Builds/${BUILD_TARGET}/${BUILD_MODE}"
        LOG_PATH="${OUTPUT_PATH}/Logs"
        BUILD_RESULT_PATH="${OUTPUT_PATH}/UnityBuildOutput"

        GCS_BUNDLES_ENDPOINT="gs://zombiescape/Bundles"
        
        ARCHIVE_PATH="${OUTPUT_PATH}/Archive"
        ARTIFACT_PATH="${ARCHIVE_PATH}/Zombiescape.ipa"
    }

    stages{
        stage('Prepare'){
            steps {
               sh '''
                   mkdir -p $OUTPUT_PATH
               '''
            }
        }
        stage('Build Unity'){
            steps {
               sh '''
                   sh BuildUnityPlayer.sh
               '''
            }
        }
        stage('Build XCode project'){
            steps {
               sh '''
                   sh IOS/BuildXCodeProject.sh
               '''
            }
        }
        stage('Archive XCode project'){
            steps {
               sh '''
                   sh IOS/ArchiveXCodeProject.sh
               '''
            }
        }
        stage('Export XCode project'){
            steps {
               sh '''
                   sh IOS/AppStoreExportXCodeProject.sh
               '''
            }
        }
        stage('Upload bundles'){
            steps {
               sh '''
                   sh GCS/UploadBundles.sh
               '''
            }
        }
    }
}
